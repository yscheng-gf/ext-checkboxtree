<?php

namespace Xn\CheckboxTree\Traits;

trait CheckboxTreeBuilder
{
    /**
     * @param \Closure $callback
     *
     * @return Grid
     */
    public static function TreeOptionBuilder(array $data, $id = 'id', $parentId = 'parent_id', $slug = 'slug', $title = 'title')
    {
        $organizedData = [];
        foreach ($data as $item) {
            $organizedData[$item[$id]] = $item;
        }

        // 遍历数据以创建层级路径
        foreach ($data as &$item) {
            $path = [];
            $currentId = $item[$id];

            while ($currentId !== "0" && isset($organizedData[$currentId])) {
                array_unshift($path, $currentId);
                $currentId = $organizedData[$currentId][$parentId];
            }
            $item['name'] = __($item[$title]);
            $item[$slug] = implode('.', $path);
        }

        return collect($data);
    }

}
