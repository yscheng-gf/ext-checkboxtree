<?php

namespace Xn\CheckboxTree\Http\Controllers;

use Xn\Admin\Layout\Content;
use Illuminate\Routing\Controller;

class CheckboxTreeController extends Controller
{
    public function index(Content $content)
    {
        return $content
            ->title('Title')
            ->description('Description')
            ->body(view('checkboxtree::index'));
    }
}
