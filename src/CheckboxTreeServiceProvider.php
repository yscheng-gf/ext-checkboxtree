<?php

namespace Xn\CheckboxTree;

use Illuminate\Support\ServiceProvider;
use Xn\Admin\Admin;
use Xn\Admin\Form;

class CheckboxTreeServiceProvider extends ServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function boot(CheckboxTreeExtension $extension)
    {
        if (! CheckboxTreeExtension::boot()) {
            return ;
        }

        $this->loadViewsFrom(resource_path('views')."/vendor/xn/checkboxtree/views", $extension->name);

        $this->registerPublishing($extension);

        Admin::booting(function () {
            // Admin::js('vendor/laravel-admin-ext/checkboxtree/dist/bootree.min.js');
            // Admin::css('vendor/laravel-admin-ext/checkboxtree/dist/bootree.min.css');
            Form::extend('checkboxtree', CheckboxTree::class);
        });

        $this->app->booted(function () {
            CheckboxTreeExtension::routes(__DIR__.'/../routes/web.php');
        });
    }

    /**
     * Register the package's publishable resources.
     *
     * @return void
     */
    protected function registerPublishing(CheckboxTreeExtension $extension)
    {
        if ($this->app->runningInConsole()) {
            $views = $extension->views();
            $assets = $extension->assets();
            $this->publishes(
                [
                    $views => resource_path('views')."/vendor/xn/checkboxtree/views",
                    $assets => public_path('vendor/laravel-admin-ext/checkboxtree')
                ], 
                $extension->name
            );
        }
    }
}
