laravel-admin Form樹狀勾選盒

安裝
```
composer require xn/checkboxtree
```

共用資源 - css/js
```
php artisan vendor:publish --tag=laravel-admin-checkboxtree
```

使用
```
$permissionModel = config('admin.database.permissions_model');
$form->checkboxtree('permissions', trans('admin.permissions'))
    ->options($permissionModel::all()->pluck('name', 'id'))
    ->treeOptions($permissionModel::orderBy('slug')->get());
```
欄位slug的組成規則
```
auth
auth.login
auth.management
auth.management.users
auth.management.roles
...
```
